Sophistication & Elegance
Moonflower is for the quintessential modern lady. She is confident, independent and passionate. 
Our dresses and outfits are designed to bring out your inner beauty. Take on your day with an effortless, sharp and on point look. 
Explore our Singapore blogshop dresses today.

Address: 10 Kaki Bukit Ave 4, #05-65 Premier @ Kaki Bukit, Singapore 415874

Phone: +65 6816 3168

Website: https://moonflowercollections.com
